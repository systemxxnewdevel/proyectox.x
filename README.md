# Creacion de Store Procedure #
# Nombre: InsertarCarroNuevo #

#USE [DBCARPRO]#
#GO#
#/****** Object:  StoredProcedure [dbo].[InsertarCarroNuevo]    Script Date: 24/01/2017 10:10:29 p.m. ******/#
#SET ANSI_NULLS ON#
#GO#
#SET QUOTED_IDENTIFIER ON#
#GO #

ALTER PROCEDURE [dbo].[InsertarCarroNuevo]
	
	@P_NombreMarca varchar(40),
	@P_Descripcion varchar(50)
AS
BEGIN
	
	insert into MarcaAuto (NombreMarca, Descripcion) 
	Values (@P_NombreMarca, @P_Descripcion)
	commit

    
END